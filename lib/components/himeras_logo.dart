import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';

class HimerasLogo extends StatefulWidget {
  @override
  _HimerasLogoState createState() => _HimerasLogoState();
}

class _HimerasLogoState extends State<HimerasLogo>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SpinningContainer(controller: _controller);
  }
}

class SpinningContainer extends AnimatedWidget {
  const SpinningContainer({Key key, AnimationController controller})
      : super(key: key, listenable: controller);

  Animation<double> get _progress => listenable;

  @override
  Widget build(BuildContext context) {
//    print((_progress.value * 255).toInt().toString());
    return Opacity(
      opacity: (0.5 - _progress.value).abs(),
      child: Image(
        image: AssetImage('images/Logo_Dark.png'),
      ),
    );
  }
}
