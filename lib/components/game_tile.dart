import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/data/models/ui/game.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:gigiapp/services/web_client.dart';

class GameTile extends StatelessWidget {
  GameTile(this.gameEntity, this.type);
  final GameEntity gameEntity;
  final String type;
//  String route;
  @override
  Widget build(BuildContext context) {
    final String name = gameEntity.name ?? "__ERROR__";
    final String imageUrl = gameEntity.image ?? ApiProvider.placeholderImage;
    return GestureDetector(
      onTap: () => type == "swipe"
          ? StoreProvider.of<AppState>(context).dispatch(
              NavigateToAction.push(
                Routes.swipe,
                arguments: {"key": gameEntity.gameKey, "type": type},
              ),
            )
          : StoreProvider.of<AppState>(context).dispatch(
              NavigateToAction.push(
                Routes.editGamePreferences,
                arguments: {"key": gameEntity.gameKey, "type": type},
              ),
            ),
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.black, Colors.white],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter),
          image: DecorationImage(
            colorFilter: ColorFilter.mode(Colors.white70, BlendMode.dstATop),
            fit: BoxFit.cover,
            image: NetworkImage(
              imageUrl,
            ),
          ),
          borderRadius: BorderRadius.circular(30),
        ),
        width: MediaQuery.of(context).size.width,
        height: 150,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 200,
              child: Text(
                name,
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
            Icon(
              Icons.chevron_right,
              size: 30,
              color: Colors.white30,
            )
          ],
        ),
      ),
    );
  }
}
