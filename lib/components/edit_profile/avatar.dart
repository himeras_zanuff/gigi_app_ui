import 'dart:io';

import 'package:christian_picker_image/christian_picker_image.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/redux/middleware/user_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class Avatar extends StatefulWidget {
  final String pictureUrl;

  const Avatar({
    Key key,
    @required this.pictureUrl,
  }) : super(key: key);

  @override
  _AvatarState createState() => _AvatarState();
}

class _AvatarState extends State<Avatar> {
  void takeImage(BuildContext context) async {
    List<File> images = await ChristianPickerImage.pickImages(
      maxImages: 1,
      enableGestures: true,
    );
//    print(images);
    StoreProvider.of<AppState>(context).dispatch(uploadUserPicture(images[0]));
    Navigator.of(context).pop();
  }

  Future _pickImage(BuildContext context) async {
    showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          takeImage(context);
          return Center();
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CircularProfileAvatar(
      StoreProvider.of<AppState>(context).state.userState.userProfile.photo ??
          StoreProvider.of<AppState>(context)
              .state
              .userState
              .facebookProfile
              .picture ??
          StoreProvider.of<AppState>(context)
              .state
              .userState
              .googleProfile
              .picture ??
          "",
      onTap: () => _pickImage(context),
      radius: 75,
//      initialsText: Text("N/A"),
      borderColor: Colors.grey[600],
      borderWidth: 5,
//      backgroundColor: Colors.transparent,
//      foregroundColor: Colors.brown.withOpacity(0.5),
    );
  }
}
