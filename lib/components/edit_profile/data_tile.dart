import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/redux/middleware/user_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class DataTile extends StatefulWidget {
  final String dataKey;
  final String value;
  final String label;
  final String type;
  DataTile({Key key, this.dataKey, this.value, this.label, this.type})
      : super(key: key);
  @override
  _DataTileState createState() => _DataTileState();
}

class _DataTileState extends State<DataTile> {
  bool isEditable = false;
  String _validValue;
  final _formKey = GlobalKey<FormState>();
  bool isValid = true;
  String value;
  @override
  Widget build(BuildContext context) {
    value = widget.value;
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      trailing: isEditable
          ? Wrap(
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.check,
                    color: Colors.green[700],
                  ),
                  onPressed: () {
                    setState(() {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        StoreProvider.of<AppState>(context).dispatch(
                            saveUserData(widget.dataKey, _validValue));
                        isEditable = false;
                      }
                    });
                  },
                ),
                IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.red[900],
                  ),
                  onPressed: () {
                    setState(() {
                      isEditable = false;
                    });
                  },
                )
              ],
            )
          : IconButton(
              icon: Icon(
                Icons.edit,
                color: Colors.grey[700],
              ),
              onPressed: () {
                setState(() {
                  isEditable = true;
                });
              },
            ),
      title: Form(
        key: _formKey,
        autovalidate: true,
        child: _getChild(
          widget.type,
        ),
      ),
      subtitle: Text(
        widget.label,
        style: TextStyle(fontSize: 12.0, color: Colors.grey[800]),
      ),
    );
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2021));
    if (picked != null)
      setState(() {
        value = picked.toString().split(' ')[0];
      });
  }

  Widget _getChild(String type) {
    switch (type) {
      case ('date'):
        return _getDateInput();
      default:
        return _getTextInput();
    }
  }

  Widget _getTextInput() => TextFormField(
        initialValue: value,
        enabled: isEditable,
        onSaved: (value) {
          _validValue = value;
        },
        validator: (value) {
          if (value.length < 5) {
            return "Please input a valid value";
          } else {
            return null;
          }
        },
        style: TextStyle(
          fontSize: 15.0,
          color: Colors.grey[300],
          fontWeight: FontWeight.w600,
        ),
      );

  Widget _getDateInput() => InkWell(
        onTap: () {
          if (isEditable) {
            _selectDate(); // Call Function that has showDatePicker()
          }
        },
        child: IgnorePointer(
          child: TextFormField(
            initialValue: value,
            enabled: isEditable,
            onSaved: (value) {
              _validValue = value;
            },
            validator: (value) {
              if (value.length < 5) {
                return "Please input a valid value";
              } else {
                return null;
              }
            },
            style: TextStyle(
              fontSize: 15.0,
              color: Colors.grey[300],
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      );
}
