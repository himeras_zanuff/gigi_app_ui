import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/data/models/navigation/navigation_item.dart';
import 'package:gigiapp/redux/actions/ui_actions.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class FooterNavBar extends StatefulWidget {
  @override
  _FooterNavBarState createState() => _FooterNavBarState();
}

class _FooterNavBarState extends State<FooterNavBar> {
  int _currentViewIndex;
  List<NavItem> navItems = NavItem.getDefaultList();
  @override
  Widget build(BuildContext context) => BottomNavigationBar(
      currentIndex:
          StoreProvider.of<AppState>(context).state.uiState.currentViewIndex,
      onTap: (index) {
        log('index ' + index.toString() + '   ' + _currentViewIndex.toString());
        setState(() {
          // ignore: unnecessary_statements
          index;
        });
        StoreProvider.of<AppState>(context).dispatch(
          NavigateToAction.replace(
            navItems[index].route,
            preNavigation: () => StoreProvider.of<AppState>(context).dispatch(
              ChangeCurrentViewIndex(index),
            ),
          ),
        );
      },
      items: navItems
          .map((NavItem e) => BottomNavigationBarItem(
                icon: Icon(e.icon),
                title: Text(e.text),
              ))
          .toList());
}
