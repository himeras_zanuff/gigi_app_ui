import 'package:gigiapp/services/web_client.dart';

Future<dynamic> handleUserData() async {
  return ApiProvider.get(
    '/api/userData/',
  );
}

Future<dynamic> handleSaveUserData(payload) async {
  return ApiProvider.put('/api/userData/', payload);
}

Future<dynamic> handleUpdateSellItem(payload) async {
  return ApiProvider.put('/api/update_sell_items/', payload);
}

Future<dynamic> handleUpdateBuyItem(payload) async {
  return ApiProvider.put('/api/update_buy_items/', payload);
}

Future<dynamic> handleUploadUserPicture(payload) async {
  return ApiProvider.uploadProfilePicture(payload);
}
