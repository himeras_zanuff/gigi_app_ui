import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart'
    hide Options;
import 'package:path/path.dart' as path;
import 'package:web_socket_channel/io.dart';

class ApiProvider {
  static final storage = new FlutterSecureStorage();
  static final Dio dio = Dio();
  static const String _host = "10.0.2.2:8000";
  static const String _baseUrl = "http://" + _host;
  static const String placeholderImage = _baseUrl + "/media/placeholder.png";

//  IOWebSocketChannel.connect('ws://10.0.2.2:8000/ws/chat/qwe/');

  static Future<IOWebSocketChannel> getWs(String chatRoom) async {
    return IOWebSocketChannel.connect(
        'ws://' + _host + '/ws/chat/' + chatRoom + '/',
        headers: await getHeaders(false));
  }
//  final String _baseUrl = "http://gigiappbe.eu-gb.mybluemix.net/api";

  static getToken() async {
    String jwtToken = await storage.read(key: 'jwt_token_access');
    return jwtToken;
  }

  static getHeaders(bool noAuth) async {
    Map<String, String> headers = {};

    String jwtToken = await getToken();
    if (jwtToken != null && !noAuth) {
      headers['Authorization'] = 'Bearer ' + jwtToken;
    }
    print(headers);
    return headers;
  }

  static Future<Response> uploadProfilePicture(File file) async {
    FormData formData = FormData.fromMap(
      {
        "file": await MultipartFile.fromFile(file.path,
            filename: path.basename(file.path)),
      },
    );
    return dio.post(
      _baseUrl + '/profilePicture/',
      data: formData,
      options: Options(
//        contentType: "application/json",
        headers: await getHeaders(false),
      ),
    );
  }

  static Future<Response> put(String url, Map<String, String> body,
      {noAuth = false}) async {
    return dio.put(
      _baseUrl + url,
      data: json.encode(body),
      options: Options(
        contentType: "application/json",
        headers: await getHeaders(noAuth),
      ),
    );
  }

  static Future<Response> post(String url, Map<String, String> body,
      {noAuth = false}) async {
    print('URL: ' + url);
    print('BODY: ' + JsonEncoder.withIndent('  ').convert(body));
    var response = await dio.post(
      _baseUrl + url,
      data: json.encode(body),
      options: Options(
        contentType: "application/json",
        headers: await getHeaders(noAuth),
      ),
    );
    return response;
  }

  static Future<Response> get(String url, {noAuth = false}) async {
    return dio.get(
      _baseUrl + url,
      options: Options(
        contentType: "application/json",
        headers: await getHeaders(noAuth),
      ),
    );
  }
}
