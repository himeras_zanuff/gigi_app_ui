import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:gigiapp/services/web_client.dart';
import 'package:google_sign_in/google_sign_in.dart';

Future<FacebookLoginResult> handleFBSignIn() async {
  FacebookLogin facebookLogin = FacebookLogin();
  FacebookLoginResult facebookLoginResult =
      await facebookLogin.logIn(['email', 'public_profile']);
  return facebookLoginResult;
}

Future<dynamic> loginFb(token) async {
  return ApiProvider.post(
      '/api/facebook_login/',
      {
        'token': token,
      },
      noAuth: true);
}

Future<dynamic> linkFb(token) async {
  return ApiProvider.post('/api/facebook_link/', {
    'token': token,
  });
}

Future<dynamic> loginG(token) async {
  return ApiProvider.post(
      '/api/google_login/',
      {
        'token': token,
      },
      noAuth: true);
}

Future<dynamic> linkG(token) async {
  return ApiProvider.post('/api/google_link/', {
    'token': token,
  });
}

Future<dynamic> storeFBData(token) async {
  return ApiProvider.post('/api/store_facebook_data/', {
    'token': token,
  });
}

Future<dynamic> storeGData(token) async {
  return ApiProvider.post('/api/store_google_data/', {
    'token': token,
  });
}

Future<GoogleSignInAccount> handleGSignIn() async {
  GoogleSignIn googleSignIn = GoogleSignIn(scopes: [
    'email',
    'profile',
  ]);
  GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  return googleSignInAccount;
}

Future<dynamic> handleEmailSignIn(email, password) async {
  return ApiProvider.post('/api/auth/jwt/create/', {
    'email': email.toLowerCase(),
    'password': password,
  });
}

Future<dynamic> handleEmailCheckIfExists(email) async {
  return ApiProvider.post('/api/auth/check_email/', {
    'email': email.toLowerCase(),
  });
}

Future<dynamic> handleCheckUserActive(email, password) async {
  return ApiProvider.post('/api/auth/jwt/create/', {
    'email': email.toLowerCase(),
    'password': password,
  });
}

Future<dynamic> handleEmailSignUp(email, password, username) async {
  return ApiProvider.post('/api/auth/users/', {
    'email': email.toLowerCase(),
    'username': username,
    'password': password,
  });
}

Future<dynamic> handleResendEmail(email) async {
  return ApiProvider.post('/api/auth/users/resend_activation/', {
    'email': email.toLowerCase(),
  });
}

Future<dynamic> handleJwtValidation(String jwtTokenAccess) async {
  return ApiProvider.post(
      '/api/auth/jwt/verify/',
      {
        'token': jwtTokenAccess,
      },
      noAuth: true);
}

Future<dynamic> handleJwtRefresh(String jwtTokenRefresh) async {
  return ApiProvider.post('/api/auth/jwt/refresh/', {
    'refresh': jwtTokenRefresh,
  });
}
