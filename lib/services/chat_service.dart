import 'package:gigiapp/services/web_client.dart';

Future<dynamic> handleConversationLoad() async {
  return ApiProvider.get('/chat/conversations/');
}

Future<dynamic> handleMessagesLoad(String conversationId) async {
  return ApiProvider.get('/chat/messages/?conversation=' + conversationId);
}
