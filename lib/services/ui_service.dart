import 'package:dio/dio.dart';
import 'package:gigiapp/services/web_client.dart';

Future<Response> handleGamesLoad() async {
  return ApiProvider.get('/api/games/');
}

Future<Response> handlePrefLoad(key) async {
  return ApiProvider.get('/api/games/' + key);
}
