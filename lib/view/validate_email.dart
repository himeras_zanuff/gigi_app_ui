import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/redux/middleware/login/email_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class ValidateEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      bottomNavigationBar: FooterNavBar(),
      body: Container(
        padding: EdgeInsets.only(top: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              alignment: Alignment.center,
              child: Text(
                "Verify your account",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
              ),
              decoration: BoxDecoration(color: Colors.blueGrey),
            ),
            Container(
              child: Text(
                "Thank you for signing up!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.white70,
                  fontSize: 24,
                ),
              ),
              padding: EdgeInsets.all(20),
            ),
            Container(
              child: Text(
                "A confirmation email has been sent you the provided email address. Please follow the instructions in the email to complete your registration.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white60,
                  fontSize: 18,
                ),
              ),
              padding: EdgeInsets.all(10),
            ),
            Icon(
              Icons.mail,
              size: 200,
            ),
            GestureDetector(
              onTap: () => StoreProvider.of<AppState>(context).dispatch(
                resendActivationEmail(),
              ),
              child: Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                decoration: BoxDecoration(
//                  color: Colors.white70,
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(
                      color: Theme.of(context).accentColor,
                    )),
                child: Text(
                  "Resend activation email",
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 24,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: GestureDetector(
                child: Text(
                  "Try different login method",
                  style: TextStyle(
                    color: Colors.white30,
                  ),
                ),
                onTap: () => StoreProvider.of<AppState>(context).dispatch(
                  NavigateToAction.replace(Routes.login),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
