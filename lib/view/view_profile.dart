import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/data/models/ui/LolUiData.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:lipsum/lipsum.dart' as lipsum;

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  LolUiData defaultLolUiData = LolUiData.initial();
  Map<String, bool> positions = {
    "TOP": false,
    "MID": false,
    "BOT": true,
    "SUPPORT": true,
    "JUNGLE": false
  };
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
          padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height / 20,
          ),
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              PersonalDataPanel(context: context),
              Container(
//                height: MediaQuery.of(context).size.height - 300,
                child: Column(
                  children: [
                    _statsRow(),
                    _achievementsRow(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () =>
                              StoreProvider.of<AppState>(context).dispatch(
                            NavigateToAction.push(
                              Routes.editPref,
                              arguments: {"type": "buy"},
                            ),
                          ),
                          child: Container(
                            margin: EdgeInsets.all(10),
                            height: 30,
                            width: MediaQuery.of(context).size.width / 3,
                            child: Center(
                              child: Text(
                                "Looking for",
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 1.5,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black54,
                                      blurRadius: 2,
                                      offset: Offset(2, 2))
                                ],
                                borderRadius: BorderRadius.circular(10),
                                color: Theme.of(context).accentColor),
                          ),
                        ),
                        GestureDetector(
                          onTap: () =>
                              StoreProvider.of<AppState>(context).dispatch(
                            NavigateToAction.push(
                              Routes.editPref,
                              arguments: {"type": "sell"},
                            ),
                          ),
                          child: Container(
                            margin: EdgeInsets.all(10),
                            height: 30,
                            width: MediaQuery.of(context).size.width / 3,
                            child: Center(
                              child: Text(
                                "Offering",
                                style: TextStyle(
                                    fontSize: 16,
                                    letterSpacing: 1.5,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black54,
                                      blurRadius: 2,
                                      offset: Offset(2, 2))
                                ],
                                borderRadius: BorderRadius.circular(10),
                                color: Theme.of(context).accentColor),
                          ),
                        ),
                      ],
                    ),
                    _lolStatsPanel(context, positions)
                  ],
                ),
              ),
            ],
          ),
        ),
//        bottomNavigationBar: FooterNavBar(),
      );
  Container _lolStatsPanel(BuildContext context, positions) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(
        left: MediaQuery.of(context).size.width / 6,
      ),
      height: 200,
      decoration: BoxDecoration(
          color: Colors.black54,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            bottomLeft: Radius.circular(30),
          )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
//                Padding(
//                  padding: EdgeInsets.only(left: 10, top: 5),
//                  child: GestureDetector(
//                    onTap: () => StoreProvider.of<AppState>(context).dispatch(
//                      NavigateToAction.push(
//                        Routes.editPref,
//                      ),
//                    ),
//                    child: Icon(
//                      Icons.edit,
//                      color: Colors.white30,
//                    ),
//                  ),
//                ),
                Text(
                  'ZanuFF',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
          Expanded(
            child: new Container(
                child: Divider(
              color: Colors.white,
              height: 0,
            )),
          ),
          _getPositions(context, positions),
          GametypeWidget()
        ],
      ),
    );
  }

  Container _achievementsRow() {
    return Container(
        padding: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 5,
        ),
        height: 100,
        child: ListView.builder(
          itemBuilder: (context, index) => AchievementWidget(),
          scrollDirection: Axis.horizontal,
        ));
  }

  Row _statsRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        StatsWidget(number: 10, title: 'Matches'),
        StatsWidget(number: 120, title: 'Liked'),
        StatsWidget(number: 30, title: 'Rejected')
      ],
    );
  }

  Widget _getPositions(context, positions) => Expanded(
        child: ListView.builder(
            reverse: true,
            scrollDirection: Axis.horizontal,
            itemCount: defaultLolUiData.roleList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Map<String, bool> newPos = positions;
                  positions[
                      defaultLolUiData.roleList.entries
                          .toList()[index]
                          .key] = !newPos[
                      defaultLolUiData.roleList.entries.toList()[index].key];
                  setState(() => {positions: newPos});
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 7,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: positions[defaultLolUiData.roleList.entries
                            .toList()[index]
                            .key]
                        ? Border.all(color: Colors.red[200])
                        : null,
                  ),
                  child: Image(
                    image: AssetImage('images/icons/lol/' +
                        defaultLolUiData.roleList.entries.toList()[index].key +
                        (positions[defaultLolUiData.roleList.entries
                                .toList()[index]
                                .key]
                            ? '_ON'
                            : '_OFF') +
                        '.png'),
                  ),
                ),
              );
            }),
      );
}

class PersonalDataPanel extends StatelessWidget {
  const PersonalDataPanel({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
                child: Image.network(
                  StoreProvider.of<AppState>(context)
                          .state
                          .userState
                          .facebookProfile
                          .picture ??
                      StoreProvider.of<AppState>(context)
                          .state
                          .userState
                          .googleProfile
                          .picture ??
                      StoreProvider.of<AppState>(context)
                          .state
                          .userState
                          .userProfile
                          .photo ??
                      "https://c7.uihere.com/files/269/467/787/desktop-wallpaper-computer-font-placeholder.jpg",
                  height: 150,
                  width: 150,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                height: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text("ZanuFF",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).accentColor,
                                fontSize: 32,
                              )),
                          Text(
                            StoreProvider.of<AppState>(context)
                                    .state
                                    .userState
                                    .userProfile
                                    .name ??
                                StoreProvider.of<AppState>(context)
                                    .state
                                    .userState
                                    .googleProfile
                                    .name ??
                                StoreProvider.of<AppState>(context)
                                    .state
                                    .userState
                                    .facebookProfile
                                    .name ??
                                "NO_NAME",
                            style: TextStyle(
                              color: Colors.white60,
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            StoreProvider.of<AppState>(context)
                                .state
                                .userState
                                .username,
                            style: TextStyle(
                              color: Colors.white60,
                              fontWeight: FontWeight.w600,
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () => StoreProvider.of<AppState>(context)
                          .dispatch(NavigateToAction.push(Routes.editProfile)),
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                          color: Colors.white70,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Edit profile",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.black54),
                            ),
                            Icon(
                              Icons.chevron_right,
                              color: Colors.black54,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ]),
      );
}

class StatsWidget extends StatelessWidget {
  const StatsWidget({this.number, this.title});
  final int number;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(number.toString(),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 32,
                  fontWeight: FontWeight.bold)),
          Text(
            title,
            style: TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }
}

class AchievementWidget extends StatelessWidget {
  const AchievementWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Icon(Icons.edit),
          Text(lipsum.createWord()),
        ],
      ),
    );
  }
}

class GametypeWidget extends StatelessWidget {
  const GametypeWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LolUiData defaultLolUiData = LolUiData.initial();
    return Expanded(
      flex: 1,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: defaultLolUiData.gametypeList.length,
        itemBuilder: (context, index) {
          return ChoiceChip(
            selected: index % 2 == 1,
            label: Text(
              defaultLolUiData.gametypeList.entries.toList()[index].key,
              style: TextStyle(
                  color: index % 2 == 1 ? Colors.white : Colors.white24),
            ),
          );
        },
      ),
    );
  }
}
