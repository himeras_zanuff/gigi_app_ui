import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/data/models/home/carousel.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/redux/middleware/login/login_thunk.dart';
import 'package:gigiapp/redux/middleware/user_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void initState() {
    super.initState();
    Store store = StoreProvider.of<AppState>(context, listen: false);
    store.dispatch(loadUserData());
  }

  List<HomeCarousel> carouselList = HomeCarousel.getDefaultList();
  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          left: false,
          right: false,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      "Welcome!",
                      style: TextStyle(
                        fontSize: 36,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    padding: EdgeInsets.only(top: 20, left: 10),
                  ),
                  GestureDetector(
                    child: Text("log out"),
                    onTap: () =>
                        StoreProvider.of<AppState>(context).dispatch(logOut()),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              CarouselSlider(
                height: MediaQuery.of(context).size.height / 3 - 30,
                items: carouselList.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return _getCard(context, i);
                    },
                  );
                }).toList(),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      "Preferences",
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
                    ),
                    padding: EdgeInsets.only(top: 10, left: 10),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FloatingActionButton(
                    onPressed: () => showProgressDialog(),
                    backgroundColor: Colors.green,
                  ),
                  FlatButton(
                    child: Text("off"),
                    color: Colors.red,
                    onPressed: () => dismissProgressDialog(),
                  ),
                  FlatButton(
                    child: Text("Find"),
                    color: Colors.red,
                    onPressed: () => Navigator.pushNamed(
                        context, Routes.editPref,
                        arguments: {"type": "swipe"}),
                  ),
                  FlatButton(
                    child: Text("Chat"),
                    color: Colors.red,
                    onPressed: () =>
                        Navigator.pushNamed(context, Routes.chatMain),
                  ),
                ],
              ),
            ],
          ),
        ),
        drawer: Container(
          width: 300,
          height: 200,
          decoration: BoxDecoration(color: Colors.red),
        ),
//          bottomNavigationBar: FooterNavBar(),
      );
}

Widget _getCard(context, HomeCarousel i) => GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          i.route,
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 3,
        child: Stack(
          children: <Widget>[
            Align(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 4),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                height: 100,
                width: 400,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)),
                child: Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              i.title,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                            ),
                            Container(
                                width: 200,
                                child: Text(
                                  i.subtitle,
                                  softWrap: true,
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 12),
                                )),
                          ],
                        ),
                        Icon(
                          Icons.chevron_right,
                          color: Colors.black26,
                          size: 50,
                        ),
                      ],
                    )),
              ),
              alignment: Alignment.bottomCenter,
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10,
                    offset: Offset(0, 5),
                  )
                ],
              ),
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: ClipRRect(
                child: Image(
                  image: AssetImage(i.image),
                ),
                borderRadius: BorderRadius.circular(20),
              ),
            ),
          ],
        ),
      ),
    );
