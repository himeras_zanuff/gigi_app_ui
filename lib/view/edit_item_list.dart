import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/data/models/ui/game_item.dart';
import 'package:gigiapp/redux/middleware/ui_thunk.dart';
import 'package:gigiapp/redux/middleware/user_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class EditGamePreference extends StatefulWidget {
  final dynamic arguments;
  const EditGamePreference({Key key, this.arguments}) : super(key: key);
  @override
  _EditGamePreferenceState createState() => _EditGamePreferenceState();
}

class _EditGamePreferenceState extends State<EditGamePreference> {
  List<Item> itemPickList;
  String gameKey;
  String type;
  Map<String, List<Item>> itemPickMap;
  Map<String, bool> selectedItems;
  @override
  void initState() {
    type = widget.arguments['type'];
    gameKey = widget.arguments['key'];
    StoreProvider.of<AppState>(context, listen: false)
        .dispatch(loadGamePreferences(gameKey));
    itemPickList = StoreProvider.of<AppState>(context, listen: false)
        .state
        .uiState
        .itemPickList;
    var itemList;
    if (type == "sell") {
      itemList = StoreProvider.of<AppState>(context, listen: false)
          .state
          .userState
          .sellList;
    } else {
      itemList = StoreProvider.of<AppState>(context, listen: false)
          .state
          .userState
          .buyList;
    }
    selectedItems = Map<String, bool>.fromIterable(
      itemList,
      key: (item) => item.code,
      value: (item) => true,
    );
    itemPickMap = itemPickList.fold(Map<String, List<Item>>(), (acc, e) {
      if (acc[e.typeDescription] != null) {
        acc[e.typeDescription].add(e);
      } else {
        acc[e.typeDescription] = [e];
      }
      return acc;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(type + selectedItems.length.toString()),
      ),
      body: ListView.builder(
        itemCount: itemPickMap.keys.length,
        itemBuilder: (context, index) => _buildItemPicker(
            selectedItems: selectedItems,
            title: itemPickMap.keys.toList()[index],
            list: itemPickMap[itemPickMap.keys.toList()[index]]),
      ),
      bottomSheet: GestureDetector(
        onTap: () {
          type == 'sell'
              ? StoreProvider.of<AppState>(context)
                  .dispatch(updateSellItem(selectedItems))
              : StoreProvider.of<AppState>(context)
                  .dispatch(updateBuyItem(selectedItems));
        },
        child: Container(
          height: 60,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.red,
          ),
          child: Center(
            child: Text(
              "Save",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 20,
                letterSpacing: 1.5,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildItemPicker(
      {String title, List<Item> list, Map<String, bool> selectedItems}) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Text(title),
          Divider(),
          Container(
            child: Wrap(
              alignment: WrapAlignment.center,
              children: list
                  .map(
                    (e) => GestureDetector(
                      onTap: () => setState(() {
                        selectedItems[e.code] = selectedItems[e.code] != null
                            ? !selectedItems[e.code]
                            : true;
                      }),
                      child: Container(
                        padding: EdgeInsets.all(2),
                        child: ChoiceChip(
                          selected: selectedItems[e.code] ?? false,
                          label: Text(e.description),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          )
        ],
      ),
    );
  }
}
