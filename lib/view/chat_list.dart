import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/data/models/ui/conversation_item.dart';
import 'package:gigiapp/redux/middleware/chat_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:gigiapp/services/web_client.dart';

class ChatMain extends StatefulWidget {
  @override
  _ChatMainState createState() => _ChatMainState();
}

class _ChatMainState extends State<ChatMain> {
  @override
  void initState() {
    StoreProvider.of<AppState>(context, listen: false)
        .dispatch(loadConversationList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, List<ConversationItem>>(
      converter: (store) => store.state.uiState.chatItemList,
      builder: (context, chatItemList) => Scaffold(
        body: ListView.separated(
            itemBuilder: (context, index) =>
                buildChatTile(context, chatItemList[index]),
            separatorBuilder: (context, index) => Divider(
                  color: Colors.black,
                ),
            itemCount: chatItemList.length),
      ),
    );
  }

  ListTile buildChatTile(BuildContext context, ConversationItem item,
      [String image = ApiProvider.placeholderImage,
      String id = 'NO_USER_SET']) {
    return ListTile(
      onTap: () {
        Navigator.pushNamed(
          context,
          Routes.individualChat,
          arguments: {
            "userId": item.user.userId,
            "conversationId": item.id,
          },
        );
      },
      leading: Image(
        image: NetworkImage(item.user.picture ?? ApiProvider.placeholderImage),
        fit: BoxFit.cover,
        width: 70,
      ),
      title: Text(
        item.user.name,
        style: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w600,
        ),
      ),
      subtitle: Text(
        item.lastMessage.text,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontSize: 14,
          color: Colors.white54,
          fontWeight: FontWeight.w600,
        ),
      ),
      trailing: Icon(Icons.chevron_right),
    );

//        Container(
//          height: 70,
//          width: 70,
//          decoration: BoxDecoration(
//            image:
//
//            color: Colors.green,
//            borderRadius: BorderRadius.circular(10),
//          ),
//        ),
//        Expanded(
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Container(
//                width: MediaQuery.of(context).size.width / 2,
//                padding: EdgeInsets.all(10),
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Text(
//                      name,
//                      style: TextStyle(
//                        fontSize: 24,
//                        fontWeight: FontWeight.w600,
//                      ),
//                    ),
//                    Wrap(
//                      children: <Widget>[
//                        Text(
//                          text,
//                          overflow: TextOverflow.ellipsis,
//                          style: TextStyle(
//                            fontSize: 14,
//                            color: Colors.white54,
//                            fontWeight: FontWeight.w600,
//                          ),
//                        )
//                      ],
//                    ),
//                  ],
//                ),
//              ),
//              Icon(Icons.chevron_right)
//            ],
//          ),
//        )
//      ],
//        );
  }
}
