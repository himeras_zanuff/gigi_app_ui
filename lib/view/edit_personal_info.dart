import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gigiapp/components/edit_profile/avatar.dart';
import 'package:gigiapp/components/edit_profile/data_tile.dart';
import 'package:gigiapp/redux/middleware/login/facebook_thunk.dart';
import 'package:gigiapp/redux/middleware/login/google_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class EditProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) => StoreConnector<AppState, ProfileModel>(
        converter: (store) => ProfileModel.fromStore(store),
        builder: (context, ProfileModel model) => Scaffold(
          body: Container(
            decoration: BoxDecoration(),
            child: buildStack(context, model),
          ),
        ),
      );
}

class ProfileModel {
  final Function() linkFacebook;
  final Function() linkGoogle;
  final String fullName;
  final String dateOfBirth;
  final String email;
  final String picture;
  final String country;
  final String city;
  final bool linkedFacebook;
  final bool linkedGoogle;

  ProfileModel(
      {this.linkFacebook,
      this.linkGoogle,
      this.fullName,
      this.picture,
      this.country,
      this.city,
      this.linkedFacebook,
      this.linkedGoogle,
      this.dateOfBirth,
      this.email});

  static ProfileModel fromStore(Store<AppState> store) {
    return ProfileModel(
      linkFacebook: () => store.dispatch(linkFacebookAction()),
      linkGoogle: () => store.dispatch(linkGoogleAction()),
      linkedFacebook:
          store.state.userState.facebookProfile?.email != null ? true : false,
      linkedGoogle:
          store.state.userState.googleProfile?.email != null ? true : false,
      fullName: store.state.userState.userProfile.name ??
          store.state.userState.googleProfile.name ??
          store.state.userState.facebookProfile.name,
      dateOfBirth: store.state.userState.userProfile.dob,
      email: store.state.userState.email,
      country: store.state.userState.userProfile.country,
      city: store.state.userState.userProfile.city,
      picture: store.state.userState.userProfile.photo ??
          store.state.userState.facebookProfile.picture ??
          store.state.userState.googleProfile.picture,
    );
  }
}

//  buildStack(context),
Widget buildStack(BuildContext context, ProfileModel model) {
  return Stack(
    children: <Widget>[
      Align(
        alignment: Alignment.bottomCenter,
        child: FractionallySizedBox(
          heightFactor: 0.8,
          child: Container(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColorDark,
            ),
            padding: EdgeInsets.only(top: 100, left: 20, right: 20),
            child: Column(
              children: [
                buildSocialPanels(model),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Container(
                      child: ListView(
                        children: [
                          DataTile(
                            dataKey: 'profile.full_name',
                            label: 'Full name',
                            value: model.fullName,
                          ),
                          DataTile(
                            dataKey: 'profile.dob',
                            label: 'Date of birth',
                            value: model.dateOfBirth,
                            type: 'date',
                          ),
                          DataTile(
                            dataKey: 'email',
                            label: 'Email',
                            value: model.email,
                          ),
                          DataTile(
                            dataKey: 'profile.country',
                            label: 'Country',
                            value: model.country,
                          ),
                          DataTile(
                            dataKey: 'profile.city',
                            label: 'City',
                            value: model.city,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            height: MediaQuery.of(context).size.height,
          ),
        ),
      ),
      Align(
        alignment: Alignment.topCenter,
        child: FractionallySizedBox(
          heightFactor: 0.2,
          child: Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 5,
                  color: Colors.grey[600],
                ),
              ),
              gradient: LinearGradient(
                colors: [Colors.black, Colors.red],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
              ),
            ),
//    borderRadius: BorderRadius.only(
//                    bottomLeft: Radius.circular(150),
//                    bottomRight: Radius.circular(150)),
//              ),
          ),
        ),
      ),
      Align(
        alignment: Alignment(0, -0.75),
        child: Avatar(pictureUrl: model.picture),
      ),
    ],
  );
}

Row buildSocialPanels(ProfileModel model) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Expanded(
        child: GestureDetector(
          onTap: () => model.linkFacebook(),
          child: Container(
            padding: EdgeInsets.all(10),
            child: Align(
              alignment: Alignment.centerRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    model.linkedFacebook
                        ? "Connected"
                        : "Link Facebook account",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white24,
                    ),
                  ),
                  Icon(FontAwesomeIcons.facebookF),
                ],
              ),
            ),
            height: 50,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color.fromARGB(255, 59, 89, 153),
                width: 2,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
//                  color: Colors.grey),
          ),
        ),
      ),
      VerticalDivider(),
      Expanded(
        child: GestureDetector(
          onTap: () => model.linkGoogle(),
          child: Container(
            padding: EdgeInsets.all(10),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(FontAwesomeIcons.google),
                  Text(
                    model.linkedGoogle ? "Connected" : "Link Google account",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white24,
                    ),
                  ),
                ],
              ),
            ),
            height: 50,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey[500], width: 2),
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
      )
    ],
  );
}
//
//showDialog(
//context: context,
//builder: (BuildContext context){
//return AlertDialog(
//title: Text("Alert Dialog"),
//content: Text("Dialog Content"),
//);
//}
//);

//showDialog(BuildContext context,String key, String value)
//{
