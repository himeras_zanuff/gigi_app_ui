import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:gigiapp/components/Login/first.dart';
import 'package:gigiapp/components/Login/textLogin.dart';
import 'package:gigiapp/components/Login/verticalText.dart';
import 'package:gigiapp/redux/middleware/login/email_thunk.dart';
import 'package:gigiapp/redux/middleware/login/facebook_thunk.dart';
import 'package:gigiapp/redux/middleware/login/google_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class Login extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: SizedBox.shrink(),
//      bottomNavigationBar: FooterNavBar(),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            tileMode: TileMode.repeated,
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color.fromARGB(255, 20, 0, 0),
              Color.fromARGB(255, 96, 0, 0)
            ],
          ),
        ),
        child: ListView(
          children: <Widget>[
            // ignore: unrelated_type_equality_checks
            Column(
              children: <Widget>[
                Row(children: <Widget>[
                  VerticalText(),
                  TextLogin(),
                ]),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _emailController,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            fillColor: Colors.lightBlueAccent,
                            labelText: 'Email',
                            labelStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                        ),
                      ),
                    ),
                    StoreConnector<AppState, bool>(
                        converter: (store) => store.state.userState.loginError,
                        builder: (context, loginError) => Visibility(
                              visible: loginError,
                              child: Text(
                                "Wrong username/password",
                              ),
                            )),
                    Padding(
                        padding:
                            const EdgeInsets.only(top: 20, left: 50, right: 50),
                        child: Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          child: TextFormField(
                            controller: _passwordController,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              labelText: 'Password',
                              labelStyle: TextStyle(
                                color: Colors.white70,
                              ),
                            ),
                          ),
                        )),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, right: 50, left: 50),
                      child: Container(
                        alignment: Alignment.bottomRight,
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 5.0,
                              spreadRadius: 1.0,
                              offset: Offset(
                                3.0,
                                3.0,
                              ),
                            ),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: FlatButton(
                          onPressed: () => StoreProvider.of<AppState>(context)
                              .dispatch(loginEmailAction(_emailController.text,
                                  _passwordController.text)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Sign In',
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 36,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: Theme.of(context).accentColor,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    FirstTime(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(children: <Widget>[
                      Expanded(
                        child: new Container(
                            margin:
                                const EdgeInsets.only(left: 10.0, right: 15.0),
                            child: Divider(
                              color: Colors.white,
                              height: 50,
                            )),
                      ),
                      Text(
                        "Other sign in options",
                        style: TextStyle(color: Colors.white70),
                      ),
                      Expanded(
                        child: new Container(
                            margin:
                                const EdgeInsets.only(left: 15.0, right: 10.0),
                            child: Divider(
                              color: Colors.white,
                              height: 50,
                            )),
                      ),
                    ]),
                    SizedBox(
                      height: 10,
                    ),
                    SignInButton(
                      Buttons.Google,
                      onPressed: () => StoreProvider.of<AppState>(context)
                          .dispatch(loginGoogleAction()),
                    ),
                    SignInButton(
                      Buttons.Facebook,
                      onPressed: () => StoreProvider.of<AppState>(context)
                          .dispatch(loginFacebookAction()),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
