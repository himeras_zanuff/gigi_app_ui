import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/components/game_tile.dart';
import 'package:gigiapp/data/models/ui/game.dart';
import 'package:gigiapp/redux/middleware/ui_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';

class GameSelect extends StatefulWidget {
  final dynamic arguments;

  const GameSelect({Key key, this.arguments}) : super(key: key);
  @override
  _GameSelectState createState() => _GameSelectState();
}

class _GameSelectState extends State<GameSelect> {
  String type;
  @override
  void initState() {
    type = widget.arguments['type'];
    StoreProvider.of<AppState>(context, listen: false).dispatch(loadGames());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 20),
        child: StoreConnector<AppState, List<GameEntity>>(
          converter: (store) => store.state.uiState.gameData,
          builder: (context, List<GameEntity> gameList) => ListView.builder(
            itemCount: gameList.length,
            padding: EdgeInsets.all(10),
            itemBuilder: (context, index) {
              return GameTile(gameList[index], type);
            },
          ),
        ),
      ),
    );
  }
}
