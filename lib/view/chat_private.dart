import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/data/models/ui/conversation_item.dart';
import 'package:gigiapp/redux/actions/chat_actions.dart';
import 'package:gigiapp/redux/middleware/chat_thunk.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:gigiapp/redux/state/user_state.dart';
import 'package:gigiapp/services/web_client.dart';
import 'package:web_socket_channel/io.dart';

class ChatPrivate extends StatefulWidget {
  final dynamic arguments;

  const ChatPrivate({Key key, this.arguments}) : super(key: key);
  @override
  _ChatPrivateState createState() => _ChatPrivateState();
}

class _ChatPrivateState extends State<ChatPrivate> {
  IOWebSocketChannel channel;
  ChatUser user;

  ScrollController controller = new ScrollController();
  @override
  void initState() {
    StoreProvider.of<AppState>(context, listen: false)
        .dispatch(loadMessages(widget.arguments['conversationId']));
    UserState userState =
        StoreProvider.of<AppState>(context, listen: false).state.userState;
    user = ChatUser(
        name: userState.userProfile.name,
        picture: userState.userProfile.photo,
        userId: userState.id);
    ApiProvider.getWs(widget.arguments['userId']).then(
      (response) {
        channel = response;
        channel.stream.listen(
          (onData) {
            StoreProvider.of<AppState>(context, listen: false).dispatch(
              RegisterNewMessage(
                payload: onData,
              ),
            );
            controller.animateTo(controller.position.maxScrollExtent,
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeOut);
          },
        );
      },
    );
    super.initState();
  }

  void onSend() {
    dynamic message = json.encode({
      "message": _textController.text,
      "sender": {
        "userId": StoreProvider.of<AppState>(context, listen: false)
            .state
            .userState
            .id,
      }
    });
    channel.sink.add(message);
    _textController.text = "";
  }

  TextEditingController _textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StoreConnector<AppState, List<Message>>(
      converter: (store) => store.state.uiState.messageItemList,
      builder: (context, chatItemList) => Container(
        child: Column(
          children: <Widget>[
            Flexible(
              child: Container(
                color: Colors.grey[900],
                padding: EdgeInsets.all(5),
                child: ListView.builder(
                  controller: controller,
                  itemBuilder: (context, index) {
                    bool isOwn = chatItemList[index].sender.userId ==
                        StoreProvider.of<AppState>(context).state.userState.id;
                    return Row(
                      mainAxisAlignment: isOwn
                          ? MainAxisAlignment.end
                          : MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(10),
                          child: Text(chatItemList[index].text),
                          decoration: BoxDecoration(
                              color: isOwn ? Colors.red : Colors.grey[700],
                              borderRadius: BorderRadius.circular(5)),
                        ),
                      ],
                    );
                  },
                  itemCount: chatItemList.length,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: TextField(
                      controller: _textController,
                      decoration: InputDecoration.collapsed(
                        hintText: "",
                        fillColor: Colors.white,
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () => onSend(),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }
}
