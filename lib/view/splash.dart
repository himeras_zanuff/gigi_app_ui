import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gigiapp/components/himeras_logo.dart';
import 'package:gigiapp/redux/middleware/login/login_thunk.dart';
import 'package:redux/redux.dart';

import '../redux/state/app_state.dart';

class Splash extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Store store = StoreProvider.of<AppState>(context, listen: false);
    store.dispatch(validateJwtToken());
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child:
//            CircularProgressIndicator(),
              HimerasLogo(),
        ),
      );
}
