import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final TextStyle msgStyle = TextStyle(fontSize: 10);
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          child: Column(
        children: <Widget>[
          Text(
            "Something went wrong",
            style: msgStyle,
          ),
          Text(
            "We are terribly sorry! Please restart the application and try again.",
            style: msgStyle,
          ),
          Text(
            "if the problem persists, please let us know via email.",
            style: msgStyle,
          ),
          Text(
            "contact@himeras.com",
            style: msgStyle,
          ),
        ],
      )),
    );
  }
}
