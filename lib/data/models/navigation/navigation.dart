import 'package:flutter/material.dart';

class Keys {
  static final navKey = new GlobalKey<NavigatorState>();
}

class Routes {
  static const homeScreen = "HOME_SCREEN";
  static const editPref = "EDIT_PREFERENCES";
  static const validateEmail = "VALIDATE_EMAIL";
  static const login = "LOGIN";
  static const profile = "PROFILE";
  static const chat = "CHAT";
  static const splash = "SPLASH";
  static const error = "ERROR";
  static const editProfile = "EDIT_PROFILE";
  static const editGamePreferences = "EDIT_GAME_PREFERENCES";
  static const chatMain = "CHAT_MAIN";
  static const individualChat = "CHAT_SINGLE";
  static const swipe = "SWIPE";
}
