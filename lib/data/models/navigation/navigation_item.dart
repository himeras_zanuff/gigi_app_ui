import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'navigation.dart';

class NavItem {
  IconData icon;
  String text;
  bool isVisible;
  String route;
  NavItem(this.icon, this.text, this.isVisible, this.route);
  static List<NavItem> getDefaultList() {
    return [
      NavItem(Icons.chat, 'Chat', true, Routes.chatMain),
      NavItem(Icons.face, 'Profile', true, Routes.profile),
      NavItem(Icons.laptop, 'Login', true, Routes.login),
      NavItem(Icons.email, 'Home', true, Routes.homeScreen),
      NavItem(Icons.label, 'Validate', true, Routes.validateEmail),
    ];
  }
}
