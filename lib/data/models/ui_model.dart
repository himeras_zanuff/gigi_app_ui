import 'package:flutter/cupertino.dart';
import 'package:gigiapp/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class UiModel {
  final int currentViewIndex;
  UiModel({
    @required this.currentViewIndex,
  });

  static UiModel fromStore(Store<AppState> store) {
    return new UiModel(
      currentViewIndex: store.state.uiState.currentViewIndex,
    );
  }
}
