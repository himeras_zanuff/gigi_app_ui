import 'package:gigiapp/data/models/navigation/navigation.dart';

final String tableCarousel = 'home_carousel';
final String columnId = '_id';
final String columnImage = 'image';
final String columnTitle = 'title';
final String columnSubtitle = 'subtitle';
final String columnRoute = 'route';

class HomeCarousel {
//  int id;
  String image;
  String title;
  String subtitle;
  String route;

  HomeCarousel({this.image, this.title, this.subtitle, this.route});
  static List<HomeCarousel> getDefaultList() {
    return [
      HomeCarousel(
        title: "Stay connected",
        subtitle: "Set up your social profile",
        image: "images/home_img/2.png",
        route: Routes.profile,
      ),
      HomeCarousel(
        title: "Find your personal healer",
        subtitle: "Start searching for fellow gamers",
        image: "images/home_img/3.png",
        route: Routes.profile,
      ),
      HomeCarousel(
        title: "Show the world your passion",
        subtitle: "Complete your profile",
        image: "images/home_img/1.png",
        route: Routes.profile,
      )
    ];
  }
}
