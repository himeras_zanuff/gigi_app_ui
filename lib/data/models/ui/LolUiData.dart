class LolUiData {
  final Map<String, Map<String, String>> roleList;
  final Map<String, Map<String, String>> gametypeList;
  final dynamic classList;

  LolUiData({
    this.roleList,
    this.gametypeList,
    this.classList,
  });
  factory LolUiData.initial() {
    return new LolUiData(
      roleList: _getRoleList(),
      gametypeList: _getGametypeList(),
      classList: _getClassList(),
    );
  }
  static Map<String, Map<String, String>> _getRoleList() => {
        "TOP": {"name": "Top lane"},
        "JUNGLE": {"name": "Jungle"},
        "MID": {"name": "Middle lane"},
        "BOT": {"name": "Botom lane"},
        "SUPPORT": {"name": "Support"},
      };

  static Map<String, Map<String, String>> _getGametypeList() => {
        "RANKED": {"name": "Ranked"},
        "DRAFT": {"name": "Regular Draft"},
        "BLIND": {"name": "Regular blind pick"},
        "ARAM": {"name": "All Random All Mid"},
        "URF": {"name": "UltraRapidFire"},
      };
  static dynamic _getClassList() => {
        "Controller": {
          "name": "Controller",
          "subClass": [
            {
              "Controller": {"name": "Controller"}
            },
            {
              "Enchanter": {"name": "Enchanter"}
            },
            {
              "Catcher": {"name": "Catcher"}
            }
          ]
        },
        "Fighter": {
          "name": "Fighter",
          "subClass": [
            {
              "Fighter": {"name": "Fighter"}
            },
            {
              "Juggernaut": {"name": "Juggernaut"}
            },
            {
              "Diver": {"name": "Diver"}
            }
          ]
        },
        "Mage": {
          "name": "Mage",
          "subClass": [
            {
              "Mage": {"name": "Mage"}
            },
            {
              "Burst": {"name": "Burst"}
            },
            {
              "Battlemage": {"name": "Battlemage"}
            },
            {
              "Artillery": {"name": "Artillery"}
            }
          ]
        },
        "Marksman": {
          "name": "Marksman",
          "subClass": [
            {
              "Marksman": {"name": "Marksman"}
            }
          ]
        },
        "Slayer": {
          "name": "Slayer",
          "subClass": [
            {
              "Slayer": {"name": "Slayer"}
            },
            {
              "Assassin": {"name": "Assassin"}
            },
            {
              "Skirmisher": {"name": "Skirmisher"}
            }
          ]
        },
        "Tank": {
          "name": "Tank",
          "subClass": [
            {
              "Tank": {"name": "Tank"}
            },
            {
              "Vanguard": {"name": "Vanguard"}
            },
            {
              "Warden": {"name": "Warden"}
            }
          ]
        },
        "Specialist": {
          "name": "Specialist",
          "subClass": [
            {
              "Specialist": {"name": "Specialist"}
            }
          ]
        }
      };
}
