class GameEntity {
  String gameKey;
  String name;
  String description;
  String image;

  GameEntity({this.gameKey, this.name, this.description, this.image});
}
