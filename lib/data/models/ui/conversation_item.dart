//{last_message: {sender: 31,
//timestamp: 2020-04-03T12:12:10.849839Z,
//content: test},
//partner: {picture: null,
//            name: null}}

class ConversationItem {
  final Message lastMessage;
  final ChatUser user;
  final String id;
  ConversationItem({this.lastMessage, this.user ,this.id});
}

class ChatUser {
  final String picture;
  final String name;
  final String userId;
  ChatUser({this.picture, this.userId, this.name});
}

class Message {
  final DateTime timestamp;
  final String text;
  ChatUser sender;
  Message({this.timestamp, this.text, this.sender});
}
