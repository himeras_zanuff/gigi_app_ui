class Item {
  final String code;
  final String description;
  final String typeDescription;
  final String type;

  Item({this.code, this.description, this.type, this.typeDescription});
}
