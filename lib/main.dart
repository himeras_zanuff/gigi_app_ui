import 'package:flutter/material.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/view/chat_list.dart';
import 'package:gigiapp/view/chat_private.dart';
import 'package:gigiapp/view/edit_item_list.dart';
import 'package:gigiapp/view/edit_personal_info.dart';
import 'package:gigiapp/view/game_select.dart';
import 'package:gigiapp/view/splash.dart';
import 'package:gigiapp/view/swipe_screen.dart';
import 'package:gigiapp/view/validate_email.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'data/models/navigation/navigation.dart';
import 'redux/reducers/app_reducer.dart';
import 'redux/state/app_state.dart';
import 'view/error.dart';
import 'view/home.dart';
import 'view/login.dart';
import 'view/view_profile.dart';

Future<void> main() async {
  final _store = Store<AppState>(
    appReducer,
    initialState: AppState.initial(),
    middleware: [
      thunkMiddleware,
      new LoggingMiddleware.printer(),
      NavigationMiddleware<AppState>(),
    ],
  );
  runApp(StoreProvider(store: _store, child: MyApp()));
}

class Keys {
  static final navKey = new GlobalKey<NavigatorState>();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProgressDialog(
      loading: Stack(
        children: [
          Opacity(
            opacity: 0.6,
            child: AbsorbPointer(
              absorbing: true,
              child: const ModalBarrier(
                dismissible: false,
                color: Colors.black,
              ),
            ),
          ),
          Center(
            child: new CircularProgressIndicator(
              backgroundColor: Colors.red,
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.grey[900]),
            ),
          ),
        ],
      ),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: NavigatorHolder.navigatorKey,
//        home: Splash(),
        initialRoute: Routes.splash,
        theme: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Colors.red[400],
          accentColor: Colors.red[900],
          primaryColorDark: Color.fromARGB(220, 20, 20, 20),
        ),
        onGenerateRoute: (RouteSettings settings) {
          var routes = <String, WidgetBuilder>{
            Routes.chatMain: (context) => ChatMain(),
            Routes.homeScreen: (context) => Home(),
            Routes.validateEmail: (context) => ValidateEmail(),
            Routes.login: (context) => Login(),
            Routes.profile: (context) => Profile(),
            Routes.splash: (context) => Splash(),
            Routes.editProfile: (context) => EditProfile(),
            Routes.error: (context) => ErrorScreen(),
            Routes.swipe: (context) =>
                SwipeScreen(arguments: settings.arguments),
            Routes.individualChat: (context) =>
                ChatPrivate(arguments: settings.arguments),
            Routes.editPref: (context) =>
                GameSelect(arguments: settings.arguments),
            Routes.editGamePreferences: (context) =>
                EditGamePreference(arguments: settings.arguments),
          };
          return MaterialPageRoute(
              builder: (context) => routes[settings.name](context));
        },
      ),
    );
  }
}
