import 'dart:io';

import 'package:gigiapp/redux/actions/user_actions.dart';
import 'package:gigiapp/services/user_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction loadUserData() {
  return (Store store) async {
    handleUserData().then((dataResponse) {
      dynamic payload = dataResponse.data[0];
      store.dispatch(StoreDataInStore(payload: payload));
    });
  };
}

ThunkAction saveUserData(String dataKey, String value) {
  return (Store store) async {
    dynamic payload = {dataKey: value};
    handleSaveUserData(payload).then((dataResponse) {
      store.dispatch(loadUserData());
    });
  };
}

ThunkAction updateSellItem(Map<String, bool> data) {
  return (Store store) async {
    Map<String, String> jsonData = data.map<String, String>((k, v) {
      return MapEntry<String, String>(k, v ? 'X' : '');
    });
    handleUpdateSellItem(jsonData).then((dataResponse) {
      store.dispatch(loadUserData());
    });
  };
}

ThunkAction updateBuyItem(Map<String, bool> data) {
  return (Store store) async {
    Map<String, String> jsonData = data.map<String, String>((k, v) {
      return MapEntry<String, String>(k, v ? 'X' : '');
    });
    handleUpdateBuyItem(jsonData).then((dataResponse) {
      store.dispatch(loadUserData());
    });
  };
}

ThunkAction uploadUserPicture(File file) {
  return (Store store) async {
    handleUploadUserPicture(file).then((dataResponse) {
      store.dispatch(loadUserData());
    });
  };
}
