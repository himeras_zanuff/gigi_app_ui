import 'package:gigiapp/redux/actions/chat_actions.dart';
import 'package:gigiapp/services/chat_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction loadConversationList() {
  return (Store store) async {
    handleConversationLoad().then((response) {
      store.dispatch(LoadConversations(payload: response));
    });
  };
}

ThunkAction loadMessages(String conversationId) {
  print('zzzzzzzzzzzzzzzzzz');
  return (Store store) async {
    handleMessagesLoad(conversationId).then((response) {
      print("response");
      print(response);
      store.dispatch(LoadMessages(payload: response));
    });
  };
}
