import 'dart:developer';

import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/redux/actions/login_actions.dart';
import 'package:gigiapp/services/login_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../ui_thunk.dart';
import 'login_thunk.dart';

ThunkAction resendActivationEmail() {
  return (Store store) async {
    new Future(() async {
      store.dispatch(startLoading());
      final storage = new FlutterSecureStorage();
      String email = await storage.read(key: 'email');
      handleResendEmail(email).then((loginResponse) {
        store.dispatch(
          stopLoading(),
        );
      }).catchError((error) {
        store.dispatch(
          stopLoading(),
        );
        store.dispatch(
          NavigateToAction.replace(
            Routes.error,
          ),
        );
      });
    });
  };
}

ThunkAction emailSignUp(email, password, username) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(startLoading());
      handleEmailSignUp(email, password, username).then((loginResponse) {
        final storage = new FlutterSecureStorage();
        storage.write(key: 'email', value: email);
        store.dispatch(
          stopLoading(),
        );
        store.dispatch(NavigateToAction.replace(
          Routes.validateEmail,
        ));
      }).catchError((error) {
        store.dispatch(
          stopLoading(),
        );
        store.dispatch(NavigateToAction.replace(
          Routes.error,
        ));
      });
    });
  };
}

ThunkAction loginEmailAction(email, password) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(startLoading());
      handleEmailSignIn(email, password).then((loginResponse) {
        final storage = new FlutterSecureStorage();
        storage.delete(key: 'email');
        store.dispatch(
          storeJwtTokens(
            access: loginResponse["access"].toString(),
            refresh: loginResponse["refresh"].toString(),
          ),
        );
        store.dispatch(
          loginSuccess(),
        );
        store.dispatch(
          stopLoading(),
        );
        store.dispatch(
          NavigateToAction.replace(
            Routes.homeScreen,
            postNavigation: () => store.dispatch(
              EmailLoginSuccessAction(
                loginResponse['access'],
              ),
            ),
          ),
        );
      }, onError: (error) {
        log("Error" + error.toString());
        handleEmailCheckIfExists(email).then((response) {
          store.dispatch(
            stopLoading(),
          );
          NavigateToAction.replace(
            Routes.validateEmail,
          );
        }, onError: (error) {});
        store.dispatch(LoginFailedAction(email: email));
        store.dispatch(stopLoading());
      });
    });
  };
}
