import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/redux/actions/login_actions.dart';
import 'package:gigiapp/services/login_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../ui_thunk.dart';

Future storeJwtTokens({String refresh, String access}) async {
  final storage = new FlutterSecureStorage();
  await storage.write(key: 'jwt_token_access', value: access);
  if (refresh != null) {
    await storage.write(key: 'jwt_token_refresh', value: refresh);
  }
}

ThunkAction loginSuccess() {
  return (Store store) async {
    new Future(() async {
      store.dispatch(
        NavigateToAction.replace(
          Routes.homeScreen,
        ),
      );
    });
  };
}

ThunkAction logOut() {
  return (Store store) async {
    new Future(() async {
      final storage = new FlutterSecureStorage();
      storage.delete(key: 'jwt_token_access');
      storage.delete(key: 'jwt_token_refresh');
      store.dispatch(
        NavigateToAction.replace(
          Routes.splash,
        ),
      );
    });
  };
}

ThunkAction validateJwtToken() {
  return (Store store) async {
    final storage = new FlutterSecureStorage();
    String jwtTokenAccess = await storage.read(key: 'jwt_token_access');
    String jwtTokenRefresh = await storage.read(key: 'jwt_token_refresh');
    if (jwtTokenRefresh == null && jwtTokenAccess == null) {
      store.dispatch(
        stopLoading(),
      );
      store.dispatch(NavigateToAction.replace(Routes.login));
    }

    new Future(() async {
      store.dispatch(startLoading());
      print("jwtTokenAccess: " + jwtTokenAccess.toString());
      handleJwtValidation(jwtTokenAccess).then((response) {
        store.dispatch(ValidationSuccessAction());
        storage.delete(key: 'email');
        store.dispatch(
          stopLoading(),
        );
        store.dispatch(
          NavigateToAction.replace(
            Routes.homeScreen,
          ),
        );
        store.dispatch(stopLoading());
      }, onError: (error) async {
        handleJwtRefresh(jwtTokenRefresh).then(
          (response) async {
            await storage.write(
                key: 'jwt_token_access', value: response['access']);
            store.dispatch(validateJwtToken());
          },
          onError: (error) async {
            storage.read(key: 'email').then(
              (str) {
                store.dispatch(
                  stopLoading(),
                );
                if (str != null) {
                  store.dispatch(
                    NavigateToAction.replace(
                      Routes.validateEmail,
                    ),
                  );
                } else {
                  store.dispatch(
                    NavigateToAction.replace(
                      Routes.login,
                    ),
                  );
                }
              },
            );
          },
        );
      }).catchError((e) {
        store.dispatch(
          stopLoading(),
        );
        NavigateToAction.replace(
          Routes.login,
        );
      });
    });
  };
}
