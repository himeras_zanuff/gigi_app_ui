import 'dart:developer';

import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:gigiapp/data/models/navigation/navigation.dart';
import 'package:gigiapp/redux/actions/login_actions.dart';
import 'package:gigiapp/services/login_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import '../ui_thunk.dart';
import 'login_thunk.dart';

ThunkAction loginGoogleAction() {
  return (Store store) async {
    new Future(() async {
      store.dispatch(startLoading());
      handleGSignIn().then((loginResponse) {
        loginResponse.authentication.then((authResponse) {
          loginG(authResponse.idToken.toString()).then(
            (response) {
              var data = response.data;
              store.dispatch(
                storeJwtTokens(
                  access: data["JWT"]["access"].toString(),
                  refresh: data["JWT"]["refresh"].toString(),
                ),
              );
              store.dispatch(
                stopLoading(),
              );
              store.dispatch(NavigateToAction.replace(
                Routes.splash,
              ));
            },
          );
        });
      }, onError: (error) {
        log("Error" + error.toString());
        store.dispatch(LoginFailedAction());
        store.dispatch(stopLoading());
      });
    });
  };
}

ThunkAction linkGoogleAction() {
  return (Store store) async {
    new Future(() async {
      store.dispatch(startLoading());
      handleGSignIn().then((loginResponse) {
        loginResponse.authentication.then((authResponse) {
          linkG(authResponse.idToken.toString()).then(
            (response) {
              var data = response.data;
              store.dispatch(
                storeJwtTokens(
                  access: data["JWT"]["access"].toString(),
                  refresh: data["JWT"]["refresh"].toString(),
                ),
              );
              store.dispatch(
                stopLoading(),
              );
              store.dispatch(NavigateToAction.replace(
                Routes.splash,
              ));
            },
          );
        });
      }, onError: (error) {
        log("Error" + error.toString());
        store.dispatch(LoginFailedAction());
        store.dispatch(stopLoading());
      });
    });
  };
}
