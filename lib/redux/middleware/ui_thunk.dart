import 'package:gigiapp/redux/actions/ui_actions.dart';
import 'package:gigiapp/services/ui_service.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

ThunkAction startLoading() {
  return (Store store) async {
//    showProgressDialog();
  };
}

ThunkAction stopLoading() {
  return (Store store) async {
//    dismissProgressDialog();
  };
}

ThunkAction loadGames() {
  return (Store store) async {
    handleGamesLoad().then((response) {
      store.dispatch(StoreGameData(response));
    });
  };
}

ThunkAction loadGamePreferences(String key) {
  return (Store store) async {
    handlePrefLoad(key).then((response) {
      store.dispatch(StoreGamePreference(response));
    });
  };
}
