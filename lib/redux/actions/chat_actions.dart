class LoadConversations {
  final dynamic payload;
  LoadConversations({this.payload});
}

class LoadMessages {
  final dynamic payload;
  LoadMessages({this.payload});
}

class RegisterNewMessage {
  final dynamic payload;
  RegisterNewMessage({this.payload});
}
