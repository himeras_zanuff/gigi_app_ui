import 'package:dio/dio.dart';

class ChangeCurrentViewIndex {
  final int index;
  ChangeCurrentViewIndex(this.index);
}

class StoreGameData {
  final Response gameData;
  StoreGameData(this.gameData);
}

class StoreGamePreference {
  final Response payload;
  StoreGamePreference(this.payload);
}
