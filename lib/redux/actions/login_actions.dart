class StartLoadingAction {
  StartLoadingAction();
}

class EmailLoginSuccessAction {
  final String payload;
  EmailLoginSuccessAction(this.payload);
}

class ResendActivationEmail {
  ResendActivationEmail();
}

class ValidationFailedAction {
  ValidationFailedAction();
}

class ValidationSuccessAction {
  ValidationSuccessAction();
}

class SignUpAction {
  final String email;
  SignUpAction({this.email});
}

class LoginFailedAction {
  final String email;
  LoginFailedAction({this.email});
}
