import 'package:flutter/material.dart';
import 'package:gigiapp/data/models/ui/LolUiData.dart';
import 'package:gigiapp/data/models/ui/conversation_item.dart';
import 'package:gigiapp/data/models/ui/game.dart';
import 'package:gigiapp/data/models/ui/game_item.dart';

class UiState {
  final int currentViewIndex;
  final LolUiData lolUiData;
  final List<GameEntity> gameData;
  final List<Item> itemPickList;
  final List<ConversationItem> chatItemList;
  final List<Message> messageItemList;
  UiState({
    this.chatItemList,
    this.itemPickList,
    this.gameData,
    this.messageItemList,
    @required this.currentViewIndex,
    @required this.lolUiData,
  });

  UiState copyWith(
      {int currentViewIndex,
      List<GameEntity> gameData,
      List<ConversationItem> chatItemList,
      List<Message> messageItemList,
      List<Item> itemPickList}) {
    return new UiState(
      chatItemList: chatItemList ?? this.chatItemList,
      messageItemList: messageItemList ?? this.messageItemList,
      gameData: gameData ?? this.gameData,
      itemPickList: itemPickList ?? this.itemPickList,
      lolUiData: this.lolUiData,
      currentViewIndex: currentViewIndex ?? this.currentViewIndex,
    );
  }

  factory UiState.initial() {
    return new UiState(
      currentViewIndex: 0,
      lolUiData: LolUiData.initial(),
      gameData: List<GameEntity>(),
      itemPickList: List<Item>(),
      chatItemList: List<ConversationItem>(),
      messageItemList: List<Message>(),
    );
  }
}
