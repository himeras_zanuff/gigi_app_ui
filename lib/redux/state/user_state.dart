import 'package:gigiapp/data/models/ui/game_item.dart';
import 'package:meta/meta.dart';

class FacebookProfile {
  final String email;
  final String id;
  final String name;
  final String picture;

  FacebookProfile({this.email, this.id, this.name, this.picture});
}

class GoogleProfile {
  final String email;
  final String name;
  final String picture;

  GoogleProfile({this.email, this.name, this.picture});
}

class UserProfile {
  final String name;
  final String dob;
  final String country;
  final String city;
  final String photo;

  UserProfile({this.name, this.dob, this.country, this.city, this.photo});
}

class UserState {
  bool isLoggedIn;
  bool isLoading;
  bool loginError;
  String id;
  FacebookProfile facebookProfile;
  GoogleProfile googleProfile;
  UserProfile userProfile;
  List<Item> sellList;
  List<Item> buyList;
  String token;
  String username;
  String email;

  UserState({
    @required this.id,
    @required this.isLoggedIn,
    @required this.isLoading,
    @required this.loginError,
    @required this.sellList,
    @required this.buyList,
    @required this.facebookProfile,
    @required this.googleProfile,
    @required this.userProfile,
    @required this.token,
    @required this.username,
    @required this.email,
  });

  factory UserState.initial() {
    return new UserState(
      id: '',
      isLoggedIn: false,
      isLoading: false,
      loginError: false,
      facebookProfile: FacebookProfile(),
      googleProfile: GoogleProfile(),
      userProfile: UserProfile(),
      token: null,
      sellList: List<Item>(),
      buyList: List<Item>(),
      username: null,
      email: null,
    );
  }

  UserState copyWith({
    bool isLoggedIn,
    bool isLoading,
    bool loginError,
    FacebookProfile facebookProfile,
    GoogleProfile googleProfile,
    UserProfile userProfile,
    List<Item> sellList,
    List<Item> buyList,
    String token,
    String id,
    String username,
    String email,
  }) {
    return new UserState(
      isLoggedIn: isLoggedIn ?? this.isLoggedIn,
      id: id ?? this.id,
      isLoading: isLoading ?? this.isLoading,
      loginError: loginError ?? this.loginError,
      facebookProfile: facebookProfile ?? this.facebookProfile,
      googleProfile: googleProfile ?? this.googleProfile,
      userProfile: userProfile ?? this.userProfile,
      token: token ?? this.token,
      sellList: sellList ?? this.sellList,
      buyList: buyList ?? this.buyList,
      username: username ?? this.username,
      email: email ?? this.email,
    );
  }

  Map toJson() {
    return {
      "email": email,
      "token": token,
      "isLoggedIn": username,
      "isLoading": isLoggedIn,
      "loginError": isLoading,
    };
  }
//
//  @override
//  bool operator ==(Object other) =>
//      identical(this, other) ||
//      other is UserState &&
//          runtimeType == other.runtimeType &&
//          isLoggedIn == other.isLoggedIn &&
//          isLoading == other.isLoading &&
//          loginError == other.loginError &&
//          fbState == other.fbState;
//
//  @override
//  int get hashCode =>
//      isLoggedIn.hashCode ^
//      isLoading.hashCode ^
//      loginError.hashCode ^
//      fbState.hashCode;
}
