import 'package:gigiapp/redux/state/ui_state.dart';
import 'package:gigiapp/redux/state/user_state.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  final UiState uiState;
  final UserState userState;
  AppState({
    @required this.uiState,
    @required this.userState,
  });

  AppState copyWith({
    UiState uiState,
    UserState userState,
  }) {
    return AppState(
      uiState: uiState ?? this.uiState,
      userState: userState ?? this.userState,
    );
  }

  Map toJson() {
    return {
      "email": this.userState.email.toString(),
      "token": this.userState.token.toString(),
      "isLoggedIn": this.userState.username.toString(),
      "isLoading": this.userState.isLoggedIn.toString(),
      "loginError": this.userState.isLoading.toString(),
    };
  }

  factory AppState.initial() {
    return AppState(
      uiState: UiState.initial(),
      userState: UserState.initial(),
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          uiState == other.uiState &&
          userState == other.userState;

  @override
  int get hashCode => uiState.hashCode ^ userState.hashCode;
}
