import 'package:gigiapp/data/models/ui/game_item.dart';
import 'package:gigiapp/redux/actions/login_actions.dart';
import 'package:gigiapp/redux/actions/user_actions.dart';
import 'package:gigiapp/redux/state/user_state.dart';
import 'package:redux/redux.dart';

final Reducer<UserState> userReducer = combineReducers<UserState>([
  TypedReducer<UserState, LoginFailedAction>(_loginFailed),
  TypedReducer<UserState, EmailLoginSuccessAction>(_emailLoginSuccess),
  TypedReducer<UserState, StoreDataInStore>(_storeDataInStore),
]);

UserState _emailLoginSuccess(UserState state, EmailLoginSuccessAction action) {
  return state.copyWith(token: action.payload);
}

UserState _loginFailed(UserState state, LoginFailedAction action) {
  return state.copyWith(
      isLoading: false, loginError: true, email: action.email);
}

UserState _storeDataInStore(UserState state, StoreDataInStore action) {
  return state.copyWith(
    isLoading: false,
    id: action.payload['id'].toString(),
    sellList: action.payload['sell'] != null
        ? action.payload['sell'].map<Item>((e) {
            return Item(
              description: e['description'],
              typeDescription: e['type_description'],
              code: e['code'],
              type: e['type'],
            );
          }).toList()
        : List<Item>(),
    buyList: action.payload['buy'] != null
        ? action.payload['buy'].map<Item>((e) {
            return Item(
              description: e['description'],
              typeDescription: e['type_description'],
              code: e['code'],
              type: e['type'],
            );
          }).toList()
        : List<Item>(),
    userProfile: UserProfile(
      name: action.payload['profile']['full_name'] ?? null,
      dob: action.payload['profile']['dob'] ?? null,
      country: action.payload['profile']['country'] ?? null,
      city: action.payload['profile']['city'] ?? null,
      photo: action.payload['profile']['photo'] ?? null,
    ),
    facebookProfile: FacebookProfile(
      email: action.payload['facebook_profile'] != null
          ? action.payload['facebook_profile']['email']
          : null,
      id: action.payload['facebook_profile'] != null
          ? action.payload['facebook_profile']['fb_id']
          : null,
      name: action.payload['facebook_profile'] != null
          ? action.payload['facebook_profile']['name']
          : null,
      picture: action.payload['facebook_profile'] != null
          ? action.payload['facebook_profile']['picture']
          : null,
    ),
    googleProfile: GoogleProfile(
      email: action.payload['google_profile'] != null
          ? action.payload['google_profile']['email']
          : null,
      name: action.payload['google_profile'] != null
          ? action.payload['google_profile']['name']
          : null,
      picture: action.payload['google_profile'] != null
          ? action.payload['google_profile']['picture']
          : null,
    ),
    username: action.payload['username'],
    email: action.payload['email'],
  );
}
