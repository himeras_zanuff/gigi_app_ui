import 'package:gigiapp/redux/reducers/ui_reducer.dart';
import 'package:gigiapp/redux/reducers/user_reducer.dart';
import 'package:gigiapp/redux/state/app_state.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    uiState: uiReducer(state.uiState, action),
    userState: userReducer(state.userState, action),
  );
}
