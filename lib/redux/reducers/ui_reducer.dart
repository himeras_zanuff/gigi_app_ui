import 'dart:convert';

import 'package:gigiapp/data/models/ui/conversation_item.dart';
import 'package:gigiapp/data/models/ui/game.dart';
import 'package:gigiapp/data/models/ui/game_item.dart';
import 'package:gigiapp/redux/actions/chat_actions.dart';
import 'package:gigiapp/redux/actions/ui_actions.dart';
import 'package:gigiapp/redux/state/ui_state.dart';
import 'package:redux/redux.dart';

final uiReducer = combineReducers<UiState>([
  TypedReducer<UiState, ChangeCurrentViewIndex>(_changeCurrentViewIndex),
  TypedReducer<UiState, StoreGameData>(_storeGameData),
  TypedReducer<UiState, StoreGamePreference>(_storeItemDataInStore),
  TypedReducer<UiState, LoadConversations>(_storeConversationsInStore),
  TypedReducer<UiState, LoadMessages>(_storeMessagesInStore),
  TypedReducer<UiState, RegisterNewMessage>(_registerNewMessage),
]);

UiState _registerNewMessage(UiState state, RegisterNewMessage action) {
  dynamic jsonData = json.decode(action.payload.toString());
  return state.copyWith(
    messageItemList: state.messageItemList
      ..add(
        Message(
          text: jsonData['message'],
          sender: ChatUser(
            userId: jsonData['sender']['userId'],
          ),
        ),
      ),
  );
}

UiState _changeCurrentViewIndex(UiState state, ChangeCurrentViewIndex action) {
  return state.copyWith(currentViewIndex: action.index);
}

UiState _storeGameData(UiState state, StoreGameData action) {
  List<GameEntity> gameList = action.gameData.data
      .map<GameEntity>((e) => GameEntity(
          name: e['name'],
          description: e['description'],
          image: e['image'],
          gameKey: e['key']))
      .toList();
  return state.copyWith(gameData: gameList);
}

UiState _storeItemDataInStore(UiState state, StoreGamePreference action) {
  List<Item> itemList = action.payload.data['item_field'].map<Item>((e) {
    return Item(
      description: e['description'],
      typeDescription: e['type_description'],
      code: e['code'],
      type: e['type'],
    );
  }).toList();
  return state.copyWith(itemPickList: itemList);
}

UiState _storeConversationsInStore(UiState state, LoadConversations action) {
  List<ConversationItem> itemList =
      action.payload.data.map<ConversationItem>((e) {
    return ConversationItem(
        id: e['id'].toString(),
        user: ChatUser(
          picture: e['partner']['picture'],
          name: e['partner']['name'],
          userId: e['partner']['id'].toString(),
        ),
        lastMessage: Message(
          timestamp: e['last_message'] != null
              ? DateTime.parse(e['last_message']['timestamp'])
              : null,
          text: e['last_message'] != null
              ? e['last_message']['message']
              : 'No message sent',
        ));
  }).toList();
  return state.copyWith(chatItemList: itemList);
//  return state;
}

UiState _storeMessagesInStore(UiState state, LoadMessages action) {
  List<Message> itemList = action.payload.data.map<Message>((e) {
    return Message(
        timestamp: DateTime.parse(e['timestamp']),
        text: e['message'],
        sender: ChatUser(
            userId: e['sender'].toString(), name: e['sender'].toString()));
  }).toList();
  return state.copyWith(messageItemList: itemList);
//  return state;
}
